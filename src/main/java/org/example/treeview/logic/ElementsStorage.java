package org.example.treeview.logic;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;

public class ElementsStorage {
    private Map<String, String> elements = new HashMap<>();
    private Map<String, String> parents = new HashMap<>();
    private String beforeParentString;
    private Consumer<String> onError = s -> {};

    public ElementsStorage(String beforeParentString, Consumer<String> onError){
        this.beforeParentString = beforeParentString;
        if (onError != null){
            this.onError = onError;
        }
    }

    public String deleteElement(String name){
        String description = elements.remove(name);
        if (description != null){
            parents.remove(name);
        }
        else {
            onError.accept("Could not delete element \"" + name + "\".");
        }
        return description;
    }

    public boolean addElement(String name, String parentName){
        if (elements.get(name) == null){
            elements.put(name, "");
            if (parentName == null){
                parentName = "";
            }
            else if (elements.get(parentName) == null) {
                onError.accept("Could not add element \"" + name + "\".");
            }
            parents.put(name, parentName);
            return true;
        }
        else {
            onError.accept("Could not add element \"" + name + "\".");
            return false;
        }
    }

    public boolean renameElement(String name, String newName){
        if (name.equals(newName)){
            return true;
        }
        if (canRenameElement(name, newName)){
            elements.put(newName, elements.remove(name));
            parents.put(newName, parents.remove(name));
            List<String> replaceParentsList = new ArrayList<>();
            parents.forEach((child, parent) -> {
                if (parent.equals(name)){
                    replaceParentsList.add(child);
                }
            });
            replaceParentsList.forEach(child -> parents.put(child, newName));
            return true;
        }
        else {
            onError.accept("Could not rename element \"" + name + "\" to \"" + newName + "\".");
            return false;
        }
    }

    public boolean canRenameElement(String name, String newName){
        return name.equals(newName) ||
                (elements.get(name) != null && elements.get(newName) == null);
    }

    public boolean setDescription(String name, String description){
        if (elements.get(name) != null){
            elements.put(name, description);
            return true;
        }
        else {
            onError.accept("Could not set description for element \"" + name + "\".");
            return false;
        }
    }

    public String getDescription(String name){
        String res = elements.get(name);
        if (res == null){
            onError.accept("Could not get description of element \"" + name + "\".");
        }
        return res;
    }

    public void save(File file) throws IOException {
        Properties props = new Properties();
        props.putAll(elements);
        parents.forEach((key, value) -> props.put("`" + key, value));
        props.store(new FileOutputStream(file), "Here could be your comment.");
    }

    public void load(File file) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(file));
        props.forEach((key, value) -> {
            String keyString = key.toString();
            String valueString = value.toString();
            if (keyString.startsWith(beforeParentString)){
                parents.put(keyString.substring(beforeParentString.length()), valueString);
            }
            else {
                elements.put(keyString, valueString);
            }
        });
    }

    public List<Element> getElementNamesTree(){
        Map<String, Element> elementMap = new HashMap<>();
        parents.forEach((childName, parentName) -> elementMap.put(childName, new Element(childName)));
        List<Element> res = new ArrayList<>();
        parents.forEach((childName, parentName) -> {
            if (parentName.isEmpty()){
                res.add(elementMap.get(childName));
            }
            else {
                elementMap.get(parentName).addChild(elementMap.get(childName));
            }
        });
        return res;
    }

    public static class Element{
        private String name;
        private List<Element> children = new ArrayList<>();

        public Element(String name) {
            this.name = name;
        }

        public void addChild(Element element){
            this.children.add(element);
        }

        public String getName() {
            return name;
        }

        public List<Element> getChildren(){
            return children;
        }
    }
}
