package org.example.treeview.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import org.example.treeview.logic.ElementsStorage;
import org.example.treeview.view.component.ChangeableTreeItem;

import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;

public class MainView extends VBox {
    private static final String defaultElementText = "New element";
    private static final String beforeParentString = "`";

    private ElementsStorage elements = new ElementsStorage(beforeParentString, System.out::println);

    @FXML
    private TreeView<String> tree;
    @FXML
    private TextField selectedItemName;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button loadButton;
    @FXML
    private Button saveButton;
    @FXML
    private TextArea description;

    private ChangeableTreeItem selectedItem;

    public MainView() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/structure/MainView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        addButton.setOnAction(e -> onAddButtonClick());
        deleteButton.setOnAction(e -> onDeleteButtonClick());
        loadButton.setOnAction(e -> onLoadButtonClick());
        saveButton.setOnAction(e -> onSaveButtonClick());
        description.textProperty().addListener((observableValue, oldText, newText) -> onElementDescriptionChanged());
        description.setDisable(true);
        selectedItemName.setDisable(true);
        selectedItemName.setEditable(false);
    }

    private void onAddButtonClick() {
        if (selectedItem == null) {
            if (tree.getRoot() == null) {
                ChangeableTreeItem root = newTreeItem(defaultElementText);
                tree.setRoot(root);
                addElement(root.getText(), null);
                setSelectedItem(root);
            }
        }
        else {
            ChangeableTreeItem root = (ChangeableTreeItem) tree.getRoot();
            int defTextCounter = 0;
            if (root.findWithText(defaultElementText) != null){
                ++defTextCounter;
                while (root.findWithText(defaultElementText + " " + defTextCounter) != null){
                    ++defTextCounter;
                }
            }
            String newElementText = defTextCounter == 0 ?
                    defaultElementText :
                    defaultElementText + " " + defTextCounter;
            selectedItem.getChildren().add(newTreeItem(newElementText));
            addElement(newElementText, selectedItem.getText());
        }
    }

    private void onDeleteButtonClick(){
        if(selectedItem != null){
            selectedItem.forEach(item -> deleteElement(item.getText()));

            ChangeableTreeItem root = (ChangeableTreeItem) tree.getRoot();
            if (root == selectedItem){
                tree.setRoot(null);
            }
            else {
                Map.Entry<Integer, Boolean> foundPrevEntry = new AbstractMap.SimpleEntry<>(null, false);
                root.forEach(item -> {
                    if (!foundPrevEntry.getValue() && item.getChildren().remove(selectedItem)){
                        foundPrevEntry.setValue(true);
                    }
                });
            }
            setSelectedItem(null);
        }
    }

    private void onLoadButtonClick(){
        try {
            elements.load(new File("./data.txt"));
            ElementsStorage.Element rootElement = elements.getElementNamesTree().get(0);
            ChangeableTreeItem root = getItemsFromElements(rootElement);
            tree.setRoot(root);
            setSelectedItem(null);
            System.out.println("Data loaded.");
        } catch (IOException e) {
            System.out.println("Could not load data to file.");
        }
    }

    private void onSaveButtonClick(){
        try {
            elements.save(new File("./data.txt"));
            System.out.println("Data saved.");
        } catch (IOException e) {
            System.out.println("Could not save data to file.");
        }
    }


    private void addElement(String name, String parent){
        elements.addElement(name, parent);
    }

    private void deleteElement(String name){
        System.out.println("delete " + name);
        elements.deleteElement(name);
    }

    private void onElementNameChange(ChangeableTreeItem item, String oldName){
        if (oldName.isBlank() || oldName.contains(beforeParentString)) {
            return;
        }
        if (item.getText().isBlank() || item.getText().contains(beforeParentString)){
            item.setText(oldName);
        }
        else {
            selectedItemName.setText(item.getText());
        }
    }

    private boolean tryRenameItem(String oldName, ChangeableTreeItem item){
        if (elements.canRenameElement(oldName, item.getText())){
            return elements.renameElement(oldName, item.getText());
        }
        else {
            return false;
        }
    }

    private void onElementDescriptionChanged(){
        if (selectedItem != null){
            elements.setDescription(selectedItem.getText(), description.getText());
        }
    }


    private void onTreeItemClick(ChangeableTreeItem item) {
        setSelectedItem(item);
    }

    private void setSelectedItem(ChangeableTreeItem item){
        selectedItem = item;
        if (item != null){
            description.setText(elements.getDescription(item.getText()));
            description.setDisable(false);
            selectedItemName.setText(item.getText());
            selectedItemName.setDisable(false);
        }
        else {
            description.setText("");
            description.setDisable(true);
            selectedItemName.setText("");
            selectedItemName.setDisable(true);
        }
    }

    private ChangeableTreeItem newTreeItem(String text){
        return new ChangeableTreeItem(text,
                this::onTreeItemClick, this::onElementNameChange, this::tryRenameItem);
    }

    private ChangeableTreeItem getItemsFromElements(ElementsStorage.Element root){
        ChangeableTreeItem res = newTreeItem(root.getName());
        for (int i = 0; i < root.getChildren().size(); i++) {
            ElementsStorage.Element child = root.getChildren().get(i);
            res.getChildren().add(getItemsFromElements(child));
        }
        return res;
    }
}
