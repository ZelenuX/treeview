package org.example.treeview.view.component;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class ChangeableTreeItem extends TreeItem<String> {
    private Label label;
    private TextField textField;
    private BiFunction<String, ChangeableTreeItem, Boolean> itemRenameTrier = (oldName, item) -> true;
    private String oldName;

    public ChangeableTreeItem(String initVal,
                              Consumer<ChangeableTreeItem> onClick,
                              BiConsumer<ChangeableTreeItem, String> onTextChange,
                              BiFunction<String, ChangeableTreeItem, Boolean> tryOnItemRename) {
        label = new Label(initVal);
        oldName = initVal;
        label.setMaxWidth(Double.POSITIVE_INFINITY);
        label.setOnMouseClicked(e -> {
            if (e.getClickCount() == 1) {
                if (onClick != null) {
                    onClick.accept(this);
                }
            } else {
                onDoubleClick();
            }
        });
        if (tryOnItemRename != null){
            this.itemRenameTrier = tryOnItemRename;
        }
        textField = new TextField(initVal);
        textField.setOnAction(event -> onTextAreaUnfocused());
        textField.focusedProperty().addListener((observableValue, lostFocus, t1) -> {
            if (lostFocus) {
                onTextAreaUnfocused();
            }
        });
        textField.textProperty().addListener((observableValue, oldS, newS) -> {
            label.setText(newS);
            onTextChange.accept(this, oldS);
        });

        this.setGraphic(label);
        this.setValue("");
    }

    public String getText(){
        return label.getText();
    }

    public void setText(String text){
        textField.setText(text);
    }

    public ChangeableTreeItem findWithText(String text){
        if (this.getText().equals(text)){
            return this;
        }
        for (ChangeableTreeItem item :
                this.getChildren().stream().map(old -> (ChangeableTreeItem)old).collect(Collectors.toList())){
            ChangeableTreeItem subItem = item.findWithText(text);
            if (subItem != null){
                return subItem;
            }
        }
        return null;
    }

    public void forEach(Consumer<ChangeableTreeItem> action){
        for (ChangeableTreeItem item :
                this.getChildren().stream().map(old -> (ChangeableTreeItem)old).collect(Collectors.toList())){
            item.forEach(action);
        }
        action.accept(this);
    }

    private void onDoubleClick() {
        textField.setText(label.getText());
        this.setGraphic(textField);
        textField.selectAll();
        textField.requestFocus();
    }

    private void onTextAreaUnfocused() {
        boolean success = itemRenameTrier.apply(oldName, this);
        if (success) {
            oldName = this.getText();
            this.setGraphic(label);
        }
        else {
            textField.requestFocus();
        }
    }
}
