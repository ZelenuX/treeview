package org.example.treeview;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.example.treeview.view.MainView;

public class MyApp extends Application {
    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(new MainView(), 800, 600));
        stage.show();
    }
}
